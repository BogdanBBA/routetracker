﻿namespace RouteTracker
{
	partial class FViewer
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FViewer));
			this.MenuButtonPanel = new System.Windows.Forms.Panel();
			this.serviceNameL = new System.Windows.Forms.Label();
			this.serviceStatsL = new System.Windows.Forms.Label();
			this.RefreshT = new System.Windows.Forms.Timer(this.components);
			this.currentTimeL = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// MenuButtonPanel
			// 
			this.MenuButtonPanel.Location = new System.Drawing.Point(410, 23);
			this.MenuButtonPanel.Name = "MenuButtonPanel";
			this.MenuButtonPanel.Size = new System.Drawing.Size(200, 60);
			this.MenuButtonPanel.TabIndex = 9;
			// 
			// serviceNameL
			// 
			this.serviceNameL.AutoSize = true;
			this.serviceNameL.Font = new System.Drawing.Font("Ubuntu", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.serviceNameL.Location = new System.Drawing.Point(26, 16);
			this.serviceNameL.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.serviceNameL.Name = "serviceNameL";
			this.serviceNameL.Size = new System.Drawing.Size(198, 34);
			this.serviceNameL.TabIndex = 8;
			this.serviceNameL.Text = "serviceNameL";
			// 
			// serviceStatsL
			// 
			this.serviceStatsL.AutoSize = true;
			this.serviceStatsL.Font = new System.Drawing.Font("Ubuntu Light", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.serviceStatsL.Location = new System.Drawing.Point(27, 58);
			this.serviceStatsL.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.serviceStatsL.Name = "serviceStatsL";
			this.serviceStatsL.Size = new System.Drawing.Size(114, 23);
			this.serviceStatsL.TabIndex = 10;
			this.serviceStatsL.Text = "serviceStatsL";
			// 
			// RefreshT
			// 
			this.RefreshT.Tick += new System.EventHandler(this.RefreshT_Tick);
			// 
			// currentTimeL
			// 
			this.currentTimeL.AutoSize = true;
			this.currentTimeL.Font = new System.Drawing.Font("Ubuntu Light", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.currentTimeL.ForeColor = System.Drawing.Color.GreenYellow;
			this.currentTimeL.Location = new System.Drawing.Point(26, 94);
			this.currentTimeL.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.currentTimeL.Name = "currentTimeL";
			this.currentTimeL.Size = new System.Drawing.Size(177, 33);
			this.currentTimeL.TabIndex = 11;
			this.currentTimeL.Text = "currentTimeL";
			// 
			// FViewer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.DimGray;
			this.ClientSize = new System.Drawing.Size(649, 433);
			this.Controls.Add(this.currentTimeL);
			this.Controls.Add(this.serviceStatsL);
			this.Controls.Add(this.MenuButtonPanel);
			this.Controls.Add(this.serviceNameL);
			this.Font = new System.Drawing.Font("Ubuntu", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ForeColor = System.Drawing.Color.White;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "FViewer";
			this.Text = "FMap";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.FMap_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel MenuButtonPanel;
		private System.Windows.Forms.Label serviceNameL;
		private System.Windows.Forms.Label serviceStatsL;
		private System.Windows.Forms.Timer RefreshT;
		private System.Windows.Forms.Label currentTimeL;


	}
}