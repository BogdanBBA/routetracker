﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RouteTracker
{
	public static class AppForms
	{
		public static Color FormBackgroundColor = ColorTranslator.FromHtml("#0E1B1F");

		public static Color ButtonColor = FormBackgroundColor;
		public static Color ButtonTextColor = ColorTranslator.FromHtml("#CAD9DB");
		public static Color SelectedButtonColor = ColorTranslator.FromHtml("#112126");
		public static Color SelectedButtonTextColor = ColorTranslator.FromHtml("#FFFFFF");
		
		public static Color SpeedBarColor = ColorTranslator.FromHtml("#4B7F82");
		public static Color VehicleBarColor = ColorTranslator.FromHtml("#BA1130");
		public static Color BracketsColor = ColorTranslator.FromHtml("#4B7F82");
		public static Color StopLabelColor = ColorTranslator.FromHtml("#B5EB2A");
		public static Color StatusLabelColor = ColorTranslator.FromHtml("#E9F5F2");
		public static Color PlaceLabelColor = ColorTranslator.FromHtml("#00BFFF");
		public static Color DataValueLabelColor = ColorTranslator.FromHtml("#E3C624");
		public static Color DataPercentageLabelColor = ColorTranslator.FromHtml("#F520AA");
		public static Color TimeLabelColor = ColorTranslator.FromHtml("#6BDE1F");

		public static Color ErrorTextColor = ColorTranslator.FromHtml("#E024A8");
		public static Color WarningTextColor = ColorTranslator.FromHtml("#E3C30B");
		public static Color OkTextColor = ColorTranslator.FromHtml("#54BD13");

		public static Size MenuButtonSize = new Size(200, 60);

		public static void InitializeAndFormatFormComponents(Form form, bool whiteMargin)
		{
			form.BackColor = FormBackgroundColor;

			foreach (Control ctrl in form.Controls)
				if (ctrl is Label)
					((Label) ctrl).BackColor = form.BackColor;
				else if (ctrl is Panel)
					((Panel) ctrl).BackColor = form.BackColor;
				else if (ctrl is PictureBox)
					((PictureBox) ctrl).BackColor = form.BackColor;

			PictureBox pic = new PictureBox();
			pic.Name = "bgImg";
			pic.Parent = form;
			PaintBackgroundImage(pic, whiteMargin);
		}

		public static void PaintBackgroundImage(PictureBox pic, bool whiteMargin)
		{
			Form f = (Form) pic.Parent;
			pic.SetBounds(0, 0, f.Width, f.Height);
			Bitmap bmp = new Bitmap(pic.Width, pic.Height);
			Graphics g = Graphics.FromImage(bmp);
			int margin = 8, border = margin + 6;
			g.FillRectangle(new SolidBrush(FormBackgroundColor), 0, 0, pic.Width, pic.Height);
			if (whiteMargin)
			{
				g.FillRectangle(new SolidBrush(Color.WhiteSmoke), margin, margin, pic.Width - 2 * margin, pic.Height - 2 * margin);
				g.FillRectangle(new SolidBrush(FormBackgroundColor), border, border, pic.Width - 2 * border, pic.Height - 2 * border);
			}
			pic.Image = bmp;
			pic.SendToBack();
		}

		public static List<PictureBoxButton> CreateMenuButtons(Panel container, List<string> captions, bool horizontal, EventHandler click)
		{
			List<PictureBoxButton> result = new List<PictureBoxButton>();
			for (int i = 0, n = captions.Count, dim = horizontal ? container.Width / n : container.Height / n; i < n; i++)
			{
				PictureBoxButton pic = new PictureBoxButton(captions[i], click);
				pic.Parent = container;
				pic.Cursor = Cursors.Hand;
				if (horizontal)
					pic.SetBounds(i * dim, 0, dim, container.Height);
				else
					pic.SetBounds(0, i * dim, container.Width, dim);
				PictureBoxButton.OnMouseLeave(pic, null);
				result.Add(pic);
			}
			return result;
		}

		public static Font GetFont(string name, int size, bool bold)
		{
			return new Font(name, size, bold ? FontStyle.Bold : FontStyle.Regular);
		}
	}

	public class PictureBoxButton : PictureBox
	{
		public string Information;

		public PictureBoxButton(string Information, EventHandler Click)
			: base()
		{
			this.Information = Information;
			this.Click += Click;
			this.MouseEnter += new EventHandler(OnMouseEnter);
			this.MouseLeave += new EventHandler(OnMouseLeave);
			OnMouseLeave(this, null);
		}

		public PictureBoxButton(string Information, EventHandler Click, EventHandler MouseEnter, EventHandler MouseLeave)
			: base()
		{
			this.Information = Information;
			this.Click += Click;
			this.MouseEnter += MouseEnter;
			this.MouseLeave += MouseLeave;
			MouseLeave(this, null);
		}

		public static void OnMouseEnter(object sender, EventArgs e)
		{
			DrawPictureBoxButton((PictureBoxButton) sender, true);
		}

		public static void OnMouseLeave(object sender, EventArgs e)
		{
			DrawPictureBoxButton((PictureBoxButton) sender, false);
		}

		private static void DrawPictureBoxButton(PictureBoxButton pic, bool selected)
		{
			if (pic.Image != null)
				pic.Image.Dispose();
			Bitmap bmp = new Bitmap(pic.Width, pic.Height);
			Graphics g = Graphics.FromImage(bmp);
			Brush bgBrush = selected ? new SolidBrush(AppForms.SelectedButtonColor) : new SolidBrush(AppForms.ButtonColor);
			Brush textBrush = selected ? new SolidBrush(AppForms.SelectedButtonTextColor) : new SolidBrush(AppForms.ButtonTextColor);
			Font font = AppForms.GetFont("Ubuntu", 16, true);

			g.FillRectangle(bgBrush, 0, 0, pic.Width, pic.Height);

			if (selected)
				g.DrawRectangle(new Pen(Color.WhiteSmoke), 2, 2, pic.Width - 4, pic.Height - 4);

			Size size = g.MeasureString(pic.Information, font).ToSize();
			g.DrawString(pic.Information, font, textBrush, new Point(pic.Width / 2 - size.Width / 2, pic.Height / 2 - size.Height / 2));

			pic.Image = bmp;
		}
	}
}
