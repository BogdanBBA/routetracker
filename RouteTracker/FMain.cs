﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace RouteTracker
{
	public partial class FMain : Form
	{
		private static List<string> MenuButtonCaptions = new List<string>(new string[3] { "Exit", "About", "View service" });

		TDatabase Database;

		public FMain()
		{
			InitializeComponent();
			AppForms.InitializeAndFormatFormComponents(this, true);
			AppForms.CreateMenuButtons(MenuButtonPanel, MenuButtonCaptions, true, MenuButton_Click);
		}

		private void FMain_Shown(object sender, EventArgs e)
		{
			Database = new TDatabase();
			string loadResult = Database.LoadDatabase();
			if (!loadResult.Equals(""))
			{
				MessageBox.Show("An error was encountered when loading the database from file:\n\n" + loadResult, "Database loading ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
				Application.Exit();
			}

			serviceListCB.Items.Clear();
			foreach (TService service in Database.Services)
				serviceListCB.Items.Add(string.Format("{0} ({1} stops)", service.Name, service.Stops.Count));
			serviceListCB.SelectedIndex = 0;
		}

		//

		public void MenuButton_Click(object sender, EventArgs e)
		{
			int r = MenuButtonCaptions.IndexOf(((PictureBoxButton) sender).Information);
			switch (r)
			{
				case 0:
					Application.Exit();
					break;
				case 1:
					MessageBox.Show("by BogdanBBA\n\nv0.1 - July 17th, 2014\nv0.2 - July 18th, 2014\nv1.0 - July 20th, 2014", "About RouteTracker", MessageBoxButtons.OK, MessageBoxIcon.Information);
					break;
				case 2:
					if (serviceListCB.SelectedIndex == -1)
					{
						MessageBox.Show("Select a project first!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						break;
					}
					new FViewer(Database.Settings, Database.Services[serviceListCB.SelectedIndex]).Show();
					break;
			}
		}
	}
}
