﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RouteTracker
{
	public partial class FViewer : Form
	{
		private static List<string> MenuButtonCaptions = new List<string>(new string[1] { "Exit" });
		private PictureBox StatusPic;
		private PictureBox GraphPic;

		private TSettings Settings;
		private TService Service;

		private List<Rectangle> RectangleList = new List<Rectangle>();
		private DateTime CurrentTime;
		private TimeSpan ServiceTimePassed;
		private double ServiceDistanceTravelled;
		private double SegmentDistanceTravelled;
		private TVehicleLocation VehicleLocation;

		public FViewer(TSettings Settings, TService Service)
		{
			InitializeComponent();
			this.Settings = Settings;
			this.Service = Service;
			AppForms.InitializeAndFormatFormComponents(this, false);
			AppForms.CreateMenuButtons(MenuButtonPanel, MenuButtonCaptions, true, MenuButton_Click);
		}

		private void CreatePictureBox(out PictureBox pic, Rectangle bounds)
		{
			pic = new PictureBox();
			pic.Parent = this;
			pic.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
			pic.BringToFront();
		}

		private void FMap_Load(object sender, EventArgs e)
		{
			MenuButtonPanel.Left = this.Width - MenuButtonPanel.Width - MenuButtonPanel.Top;
			CreatePictureBox(out StatusPic, new Rectangle(serviceNameL.Left, currentTimeL.Bottom + 16, this.Width - 2 * serviceNameL.Left, 170));
			CreatePictureBox(out GraphPic, new Rectangle(serviceNameL.Left, StatusPic.Bottom + 16, StatusPic.Width, this.Height - StatusPic.Bottom - 16));
			currentTimeL.ForeColor = AppForms.TimeLabelColor;

			Service.WriteToFile();
			serviceNameL.Text = Service.Name;
			serviceStatsL.Text = string.Format("Route: \"{0}\"; stops: {1}; distance: {2} KM; time: {3}",
				Service.Route.Name, Service.Stops.Count, Service.Totals.Distance.ToString("0.0"),
				TDatabase.FormatTimeSpan(Service.Totals.Time));

			RefreshT.Interval = Settings.TickInterval;
			RefreshT_Tick(null, null);
			RefreshT.Enabled = true;
		}

		//

		public void MenuButton_Click(object sender, EventArgs e)
		{
			int r = MenuButtonCaptions.IndexOf(((PictureBoxButton) sender).Information);
			switch (r)
			{
				case 0:
					this.Dispose();
					break;
			}
		}

		/// 
		/// 
		/// 

		private TVehicleLocation GetVehicleLocation()
		{
			TVehicleLocation vehLoc = null;
			foreach (TStop stop in Service.Stops)
				if (CurrentTime.TimeOfDay.CompareTo(stop.Arrival.TimeOfDay) > 0 && CurrentTime.TimeOfDay.CompareTo(stop.Departure.TimeOfDay) <= 0)
					vehLoc = stop;
			if (vehLoc != null)
				return vehLoc;
			foreach (TSegment segment in Service.Segments)
				if (CurrentTime.TimeOfDay.CompareTo(segment.From.Departure.TimeOfDay) > 0 && CurrentTime.TimeOfDay.CompareTo(segment.To.Arrival.TimeOfDay) <= 0)
					vehLoc = segment;
			return vehLoc;
		}

		private void RefreshT_Tick(object sender, EventArgs e)
		{
			CurrentTime = DateTime.Now.Add(Settings.TimeOffset);
			CurrentTime = new DateTime(2000, 1, 1, CurrentTime.Hour, CurrentTime.Minute, CurrentTime.Second);
			currentTimeL.Text = "Current time: " + TDatabase.EncodeTime(CurrentTime);
			VehicleLocation = GetVehicleLocation();
			DrawStatus(VehicleLocation);
			DrawGraph(VehicleLocation, CurrentTime);
		}

		private Bitmap PreparePictureBox(PictureBox pic)
		{
			if (pic.Image != null)
				pic.Image.Dispose();
			return new Bitmap(pic.Width, pic.Height);
		}

		/// 
		/// 
		/// 

		private void DrawLabel(Graphics g, Font font, Brush brush, ref int x, int y, string text, bool drawShadow)
		{
			if (Settings.DrawStatusLabelRectangles)
				g.DrawRectangle(new Pen(Color.Gray), new Rectangle(new Point(x, y), g.MeasureString(text, font).ToSize()));
			if (drawShadow)
				g.DrawString(text, font, new SolidBrush(Color.Black), x + 1, y + 1);
			g.DrawString(text, font, brush, x, y);
			x += (int) g.MeasureString(text, font).Width;
		}

		private void CalculateServiceTimePassedAndDistanceTravelled()
		{
			ServiceTimePassed = new TimeSpan(0, 0, 0, 0);
			if (VehicleLocation == null)
				return;

			TSegment segment;
			TStop stop;
			SegmentDistanceTravelled = 0;
			if (VehicleLocation is TSegment)
			{
				segment = VehicleLocation as TSegment;
				ServiceTimePassed = ServiceTimePassed.Add(TService.TimeSpanSince(segment.From.Departure, CurrentTime));
				SegmentDistanceTravelled = (double) ((double) TService.TimePassedPercentage(ServiceTimePassed, segment.Time) * segment.Distance) / 100;
			}
			else
			{
				stop = VehicleLocation as TStop;
				ServiceTimePassed = ServiceTimePassed.Add(TService.TimeSpanSince(stop.Arrival, CurrentTime));
			}
			ServiceDistanceTravelled = SegmentDistanceTravelled;

			stop = VehicleLocation.PreviousStop;
			segment = VehicleLocation.PreviousSegment;
			while (stop != null)
			{
				ServiceTimePassed = ServiceTimePassed.Add(stop.StoppageTime);
				stop = stop.PreviousStop;
			}
			while (segment != null)
			{
				ServiceTimePassed = ServiceTimePassed.Add(segment.Time);
				ServiceDistanceTravelled += segment.Distance;
				segment = segment.PreviousSegment;
			}
		}

		private void DrawStatus(TVehicleLocation vehLocation)
		{
			Bitmap bmp = PreparePictureBox(StatusPic);
			Graphics g = Graphics.FromImage(bmp);
			Brush brushA, brushB, brushC;
			Font fontA, fontB;

			// The service is currently inactive
			if (vehLocation == null)
			{
				brushA = new SolidBrush(Color.WhiteSmoke);
				fontA = AppForms.GetFont("Ubuntu", 16, true);
				g.DrawString("The service is currently inactive!", fontA, brushA, new Point(48, 16));
			}
			// The service is running
			else
			{
				CalculateServiceTimePassedAndDistanceTravelled();
				int x, y;
				// It'result on a segment
				if (vehLocation is TSegment)
				{
					TSegment segment = vehLocation as TSegment;
					//first line
					x = 4;
					y = 4;
					fontA = AppForms.GetFont("Ubuntu", 15, false);
					fontB = AppForms.GetFont("Ubuntu", 17, true);
					brushA = new SolidBrush(AppForms.StatusLabelColor);
					brushB = new SolidBrush(AppForms.PlaceLabelColor);
					DrawLabel(g, fontA, brushA, ref x, y, "The " + Service.VehicleName + " has departed from", false);
					DrawLabel(g, fontB, brushB, ref x, y, segment.From.Point.Name, true);
					DrawLabel(g, fontA, brushA, ref x, y, "and is going to arrive in", false);
					DrawLabel(g, fontB, brushB, ref x, y, segment.To.Point.Name, true);
					DrawLabel(g, fontA, brushA, ref x, y, "at", false);
					DrawLabel(g, fontB, new SolidBrush(AppForms.TimeLabelColor), ref x, y, TDatabase.EncodeTime(segment.To.Arrival), true);
					DrawLabel(g, fontA, brushA, ref x, y, " (", false);
					DrawLabel(g, fontB, new SolidBrush(AppForms.DataValueLabelColor), ref x, y, TDatabase.FormatTimeSpan(TService.TimeSpanSince(CurrentTime, segment.To.Arrival)), true);
					DrawLabel(g, fontA, brushA, ref x, y, "left ) .", false);
					//second line
					x = 4;
					y = 42;
					fontA = AppForms.GetFont("Ubuntu", 13, false);
					fontB = AppForms.GetFont("Ubuntu", 13, true);
					brushA = new SolidBrush(AppForms.StatusLabelColor);
					brushB = new SolidBrush(AppForms.DataValueLabelColor);
					brushC = new SolidBrush(AppForms.DataPercentageLabelColor);
					DrawLabel(g, fontA, brushA, ref x, y, "So far, it has travelled", false);
					DrawLabel(g, fontA, brushB, ref x, y, ServiceDistanceTravelled.ToString("0.0") + " km", true);
					DrawLabel(g, fontA, brushA, ref x, y, "of", false);
					DrawLabel(g, fontB, brushB, ref x, y, Service.Totals.Distance.ToString("0.0") + " km", true);
					DrawLabel(g, fontA, brushA, ref x, y, " (", false);
					DrawLabel(g, fontA, brushC, ref x, y, ((double) (ServiceDistanceTravelled * 100) / Service.Totals.Distance).ToString("0.0") + "%", true);
					DrawLabel(g, fontA, brushA, ref x, y, ")  for", false);
					DrawLabel(g, fontA, brushB, ref x, y, TDatabase.FormatTimeSpan(ServiceTimePassed), true);
					DrawLabel(g, fontA, brushA, ref x, y, "of the total", false);
					DrawLabel(g, fontB, brushB, ref x, y, TDatabase.FormatTimeSpan(Service.Totals.Time), true);
					DrawLabel(g, fontA, brushA, ref x, y, " (", false);
					DrawLabel(g, fontA, brushC, ref x, y, TService.TimePassedPercentage(ServiceTimePassed, Service.Totals.Time).ToString("0.0") + "%", true);
					DrawLabel(g, fontA, brushA, ref x, y, " ) .", false);
					//third line
					x = 48;
					y = 82;
					fontA = AppForms.GetFont("Ubuntu", 12, false);
					DrawLabel(g, fontA, brushA, ref x, y, "The average speed for this segment is", false);
					DrawLabel(g, fontA, brushB, ref x, y, segment.AvgSpeed.ToString("0.0") + " km/h", true);
					DrawLabel(g, fontA, brushA, ref x, y, " (", false);
					DrawLabel(g, fontA, brushC, ref x, y, ((double) (segment.AvgSpeed * 100) / Service.Totals.AvgSpeed).ToString("0") + "%", true);
					DrawLabel(g, fontA, brushA, ref x, y, "compared to the overall average ) .", false);
					//fourth line
					x = 48;
					y = 108;
					fontA = AppForms.GetFont("Ubuntu", 12, false);
					fontB = AppForms.GetFont("Ubuntu", 12, true);
					DrawLabel(g, fontA, brushA, ref x, y, "The " + Service.VehicleName + " has now travelled", false);
					DrawLabel(g, fontA, brushB, ref x, y, (SegmentDistanceTravelled * 1000).ToString("0") + " m", true);
					DrawLabel(g, fontA, brushA, ref x, y, "of the segment's", false);
					DrawLabel(g, fontA, brushB, ref x, y, segment.Distance.ToString("0.00") + " km", true);
					DrawLabel(g, fontA, brushA, ref x, y, " (", false);
					DrawLabel(g, fontA, brushC, ref x, y, ((double) (SegmentDistanceTravelled * 100) / segment.Distance).ToString("0.00") + "%", true);
					DrawLabel(g, fontA, brushA, ref x, y, " )  since", false);
					DrawLabel(g, fontB, new SolidBrush(AppForms.TimeLabelColor), ref x, y, TDatabase.EncodeTime(segment.From.Departure), true);
					DrawLabel(g, fontA, brushA, ref x, y, " .", false);
					//fifth line
					x = 48;
					y = 134;
					fontA = AppForms.GetFont("Ubuntu", 12, false);
					DrawLabel(g, fontA, brushA, ref x, y, "This segment represents", false);
					DrawLabel(g, fontA, brushC, ref x, y, ((double) (segment.Distance * 100) / Service.Totals.Distance).ToString("0.00") + "%", true);
					DrawLabel(g, fontA, brushA, ref x, y, "of the total distance, and takes", false);
					DrawLabel(g, fontA, brushB, ref x, y, TDatabase.FormatTimeSpan(segment.Time), true);
					DrawLabel(g, fontA, brushA, ref x, y, "or", false);
					DrawLabel(g, fontA, brushC, ref x, y, ((double) (segment.Time.TotalHours * 100) / Service.Totals.Time.TotalHours).ToString("0.00") + "%", true);
					DrawLabel(g, fontA, brushA, ref x, y, "of the total travel time .", false);
				}
				// It'result in a stop
				else
				{
					TStop stop = vehLocation as TStop;
					//first line
					x = 4;
					y = 4;
					fontA = AppForms.GetFont("Ubuntu", 15, false);
					fontB = AppForms.GetFont("Ubuntu", 16, true);
					brushA = new SolidBrush(AppForms.StatusLabelColor);
					brushB = new SolidBrush(AppForms.PlaceLabelColor);
					brushC = new SolidBrush(AppForms.DataValueLabelColor);
					DrawLabel(g, fontA, brushA, ref x, y, "The " + Service.VehicleName + " is currently stationing in", false);
					DrawLabel(g, fontB, brushB, ref x, y, stop.Point.Name, true);
					DrawLabel(g, fontA, brushA, ref x, y, " ( for another", false);
					DrawLabel(g, fontB, brushC, ref x, y, TDatabase.FormatTimeSpan(TService.TimeSpanSince(CurrentTime, stop.Departure)), true);
					DrawLabel(g, fontA, brushA, ref x, y, ") .", false);
					//second line
					x = 4;
					y = 42;
					fontA = AppForms.GetFont("Ubuntu", 13, false);
					fontB = AppForms.GetFont("Ubuntu", 13, true);
					brushA = new SolidBrush(AppForms.StatusLabelColor);
					brushB = new SolidBrush(AppForms.DataValueLabelColor);
					brushC = new SolidBrush(AppForms.DataPercentageLabelColor);
					DrawLabel(g, fontA, brushA, ref x, y, "So far, it has travelled", false);
					DrawLabel(g, fontA, brushB, ref x, y, ServiceDistanceTravelled.ToString("0.0") + " km", true);
					DrawLabel(g, fontA, brushA, ref x, y, "of", false);
					DrawLabel(g, fontB, brushB, ref x, y, Service.Totals.Distance.ToString("0.0") + " km", true);
					DrawLabel(g, fontA, brushA, ref x, y, " (", false);
					DrawLabel(g, fontA, brushC, ref x, y, ((double) (ServiceDistanceTravelled * 100) / Service.Totals.Distance).ToString("0.0") + "%", true);
					DrawLabel(g, fontA, brushA, ref x, y, ")  for", false);
					DrawLabel(g, fontA, brushB, ref x, y, TDatabase.FormatTimeSpan(ServiceTimePassed), true);
					DrawLabel(g, fontA, brushA, ref x, y, "of the total", false);
					DrawLabel(g, fontB, brushB, ref x, y, TDatabase.FormatTimeSpan(Service.Totals.Time), true);
					DrawLabel(g, fontA, brushA, ref x, y, " (", false);
					DrawLabel(g, fontA, brushC, ref x, y, TService.TimePassedPercentage(ServiceTimePassed, Service.Totals.Time).ToString("0.0") + "%", true);
					DrawLabel(g, fontA, brushA, ref x, y, " ) .", false);
					//third line
					x = 48;
					y = 82;
					brushA = new SolidBrush(AppForms.StatusLabelColor);
					brushB = new SolidBrush(AppForms.PlaceLabelColor);
					brushC = new SolidBrush(AppForms.TimeLabelColor);
					fontA = AppForms.GetFont("Ubuntu", 12, false);
					fontB = AppForms.GetFont("Ubuntu", 12, true);
					if (stop.PreviousStop == null)
						DrawLabel(g, fontA, brushA, ref x, y, "This is the departure point for the service.", false);
					else
					{
						DrawLabel(g, fontA, brushA, ref x, y, "It arrived from", false);
						DrawLabel(g, fontB, brushB, ref x, y, stop.PreviousStop.Point.Name, true);
						DrawLabel(g, fontA, brushA, ref x, y, "at", false);
						DrawLabel(g, fontB, brushC, ref x, y, TDatabase.EncodeTime(stop.Arrival), true);
						DrawLabel(g, fontA, brushA, ref x, y, ".", false);
					}
					//fourth line
					x = 48;
					y = 108;
					fontA = AppForms.GetFont("Ubuntu", 12, false);
					fontB = AppForms.GetFont("Ubuntu", 12, true);
					if (stop.NextStop == null)
						DrawLabel(g, fontA, brushA, ref x, y, "This is the final station of the service.", false);
					else
					{
						DrawLabel(g, fontA, brushA, ref x, y, "It departs at", false);
						DrawLabel(g, fontB, brushC, ref x, y, TDatabase.EncodeTime(stop.Departure), true);
						DrawLabel(g, fontA, brushA, ref x, y, ",  bound for", false);
						DrawLabel(g, fontB, brushB, ref x, y, stop.NextStop.Point.Name, true);
						DrawLabel(g, fontA, brushA, ref x, y, ".", false);
					}
				}
			}

			StatusPic.Image = bmp;
		}

		/// 
		/// 
		/// 

		private double GetTimePassedPercentage(DateTime moment)
		{
			return TService.TimePassedPercentage(TService.TimeSpanSince(Service.Totals.From.Arrival, moment), Service.Totals.Time);
		}

		private int GetGraphXCoord(DateTime moment)
		{
			return (int) ((double) GetTimePassedPercentage(moment) * GraphPic.Width) / 100;
		}

		private int GetGraphYCoord(double currAvgSpeed, int speedBarBottom)
		{
			double ofMaxSpeed = (double) currAvgSpeed / Service.MaximumAvgSpeed;
			return (int) ((double) ofMaxSpeed * speedBarBottom);
		}

		private bool RectangleIntersectsAnotherRectangle(Rectangle rectangleOfInterest, List<Rectangle> rectList)
		{
			foreach (Rectangle rect in rectList)
				if (rectangleOfInterest.IntersectsWith(rect))
					return true;
			return false;
		}

		private Rectangle GetNewLabelRectangle(List<Rectangle> rectangles, int x, int initialY, Size bounds, int rightPadding, string text, Font font, Graphics g)
		{
			Rectangle result = new Rectangle();
			Size textSize = g.MeasureString(text, font).ToSize();

			while (initialY + textSize.Height <= bounds.Height)
			{
				result = new Rectangle(new Point(x, initialY), textSize);
				result.Width = result.Width + rightPadding;
				if (result.Right > bounds.Width)
					result.X = result.Left - (result.Right - bounds.Width);

				if (!RectangleIntersectsAnotherRectangle(result, rectangles))
					break;
				else
					initialY += textSize.Height + 4;
			}

			rectangles.Add(result);
			return result;
		}

		private void DrawGraph(TVehicleLocation vehLocation, DateTime currentTime)
		{
			Bitmap bmp = PreparePictureBox(GraphPic);
			Graphics g = Graphics.FromImage(bmp);
			Brush brush;
			Pen pen;
			Font font;
			RectangleList.Clear();

			// Coordinates and dimensions 
			int speedBarBottom = 3 * GraphPic.Height / 5;
			int vehicleBarTop = speedBarBottom + 4, vehicleBarHeight = 20, vehicleBarBottom = vehicleBarTop + vehicleBarHeight;
			int stopBracketsTop = vehicleBarBottom + 4, stopBracketsHeight = 4, stopBracketsBottom = stopBracketsTop + stopBracketsHeight, bracketEndWidth = 4;
			int labelsTop = stopBracketsBottom + 20;

			// Segments
			brush = new SolidBrush(AppForms.SpeedBarColor);
			foreach (TSegment segment in Service.Segments)
			{
				int x = GetGraphXCoord(segment.From.Departure);
				int w = GetGraphXCoord(segment.To.Arrival) - x;
				int h = GetGraphYCoord(segment.AvgSpeed, speedBarBottom);
				int y = speedBarBottom - h;
				//MessageBox.Show(string.Format("Total time: {4}\n\nx = {0}; w = {1}\ny = {2}; h = {3}", x, w, y, h, TDatabase.FormatTimeSpan(Service.Totals.Time, true)));
				g.FillRectangle(brush, x, y, w, h);
			}

			// Stops
			pen = new Pen(AppForms.BracketsColor);
			brush = new SolidBrush(AppForms.StopLabelColor);
			font = AppForms.GetFont("Ubuntu", 10, false);
			foreach (TStop stop in Service.Stops)
			{
				int x1 = Math.Max(GetGraphXCoord(stop.Arrival) - 1, 0), x2 = GetGraphXCoord(stop.Departure), xM = (x1 + x2) / 2;
				while (x2 >= GraphPic.Width)
					x2--;
				g.DrawLine(pen, x1, stopBracketsTop, x1, stopBracketsBottom);
				g.DrawLine(pen, x2, stopBracketsTop, x2, stopBracketsBottom);
				g.DrawLine(pen, x1, stopBracketsBottom, x2, stopBracketsBottom);
				//
				stop.GraphRectangle = GetNewLabelRectangle(RectangleList, xM + bracketEndWidth + 2, labelsTop, GraphPic.Size, bracketEndWidth, stop.Point.Name, font, g);
				//
				int xMR = stop.GraphRectangle.Left >= xM ? stop.GraphRectangle.Top + stop.GraphRectangle.Height / 2 : stop.GraphRectangle.Top;
				g.DrawLine(pen, xM, stopBracketsBottom, xM, xMR);
				if (stop.GraphRectangle.Left >= xM)
					g.DrawLine(pen, xM, xMR, xM + bracketEndWidth, xMR);
			}
			foreach (TStop stop in Service.Stops)
			{
				g.DrawString(stop.Point.Name, font, new SolidBrush(Color.Black), stop.GraphRectangle.Left + 1, stop.GraphRectangle.Top + 1);
				g.DrawString(stop.Point.Name, font, brush, stop.GraphRectangle.Left, stop.GraphRectangle.Top);
			}

			// Vehicle location
			if (vehLocation != null)
			{
				int x = GetGraphXCoord(new DateTime(2000, 1, 1, currentTime.Hour, currentTime.Minute, currentTime.Second));
				brush = new SolidBrush(AppForms.VehicleBarColor);
				g.FillRectangle(brush, 0, vehicleBarTop, x, vehicleBarHeight);
			}

			// Finish
			GraphPic.Image = bmp;
		}
	}
}
