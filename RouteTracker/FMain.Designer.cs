﻿namespace RouteTracker
{
	partial class FMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMain));
			this.label1 = new System.Windows.Forms.Label();
			this.serviceListCB = new System.Windows.Forms.ComboBox();
			this.zoomT = new System.Windows.Forms.Timer(this.components);
			this.MenuButtonPanel = new System.Windows.Forms.Panel();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Ubuntu", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(41, 32);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 27);
			this.label1.TabIndex = 4;
			this.label1.Text = "Services";
			// 
			// serviceListCB
			// 
			this.serviceListCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.serviceListCB.Font = new System.Drawing.Font("Ubuntu", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.serviceListCB.FormattingEnabled = true;
			this.serviceListCB.Location = new System.Drawing.Point(46, 63);
			this.serviceListCB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.serviceListCB.Name = "serviceListCB";
			this.serviceListCB.Size = new System.Drawing.Size(689, 32);
			this.serviceListCB.TabIndex = 5;
			// 
			// zoomT
			// 
			this.zoomT.Interval = 600;
			// 
			// MenuButtonPanel
			// 
			this.MenuButtonPanel.Location = new System.Drawing.Point(89, 121);
			this.MenuButtonPanel.Name = "MenuButtonPanel";
			this.MenuButtonPanel.Size = new System.Drawing.Size(600, 60);
			this.MenuButtonPanel.TabIndex = 7;
			// 
			// FMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.ClientSize = new System.Drawing.Size(786, 233);
			this.ControlBox = false;
			this.Controls.Add(this.MenuButtonPanel);
			this.Controls.Add(this.serviceListCB);
			this.Controls.Add(this.label1);
			this.Font = new System.Drawing.Font("Ubuntu", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ForeColor = System.Drawing.Color.White;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "FMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Route Tracker";
			this.Shown += new System.EventHandler(this.FMain_Shown);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox serviceListCB;
		private System.Windows.Forms.Timer zoomT;
		private System.Windows.Forms.Panel MenuButtonPanel;

	}
}

