﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace RouteTracker
{
	public class TDatabase
	{
		private const string DataSourceFilePath = "data.xml";

		public TSettings Settings;
		public List<TRoute> Routes;
		public List<TService> Services;

		public string LoadDatabase()
		{
			string phase = "initializing";
			try
			{
				Settings = new TSettings();
				Routes = new List<TRoute>();
				Services = new List<TService>();

				XmlDocument doc = new XmlDocument();
				XmlNode node = null;

				phase = "loading xml";
				doc.Load(DataSourceFilePath);

				phase = "decoding settings"; // it'service "doc.SelectSingleNode" because "node=..., node.SelectSingleNode" doesn'timeSpan work
				Settings.TickInterval = Int32.Parse(doc.SelectSingleNode("/RouteTrackerProject/Settings/TickInterval").Attributes["value"].Value);
				Settings.TimeOffset = DecodeTimeOffset(doc.SelectSingleNode("/RouteTrackerProject/Settings/TimeOffset").Attributes["value"].Value);
				Settings.DrawStatusLabelRectangles = Boolean.Parse(doc.SelectSingleNode("/RouteTrackerProject/Settings/DrawStatusLabelRectangles").Attributes["value"].Value);

				phase = "decoding routes";
				node = doc.SelectSingleNode("/RouteTrackerProject/Routes");
				foreach (XmlNode routeNode in node.ChildNodes)
				{
					TRoute route = new TRoute(routeNode.Attributes["ID"].Value, routeNode.Attributes["name"].Value);
					phase = "decoding route '" + route.Name + "'";
					foreach (XmlNode pointNode in routeNode.ChildNodes)
					{
						TPoint point = new TPoint(pointNode.Attributes["ID"].Value, pointNode.Attributes["name"].Value, Double.Parse(pointNode.Attributes["distance"].Value));
						route.Points.Add(point);
					}
					Routes.Add(route);
				}

				phase = "decoding services";
				node = doc.SelectSingleNode("/RouteTrackerProject/Services");
				foreach (XmlNode serviceNode in node.ChildNodes)
				{
					phase = "decoding service route";
					TRoute route = this.GetRouteByID(serviceNode.Attributes["routeID"].Value);
					if (route == null)
						throw new ApplicationException("Invalid route ID '" + serviceNode.Attributes["routeID"].Value + "'");
					TService service = new TService(serviceNode.Attributes["ID"].Value, serviceNode.Attributes["name"].Value, serviceNode.Attributes["vehicle"].Value, route);
					phase = "decoding service '" + service.Name + "'";
					foreach (XmlNode stopNode in serviceNode.ChildNodes)
					{
						TPoint point = route.GetPointByID(stopNode.Attributes["pointID"].Value);
						TStop stop = new TStop(point, DecodeTime(stopNode.Attributes["arrival"].Value), DecodeTime(stopNode.Attributes["departure"].Value));
						service.Stops.Add(stop);
					}
					service.GenerateSegments();
					Services.Add(service);
				}
			}
			catch (Exception E)
			{
				return string.Format(" ### Phase: '{0}'\n\n ### Error message: '{1}'", phase, E.Message);
			}
			return "";
		}

		public TRoute GetRouteByID(string ID)
		{
			foreach (TRoute route in Routes)
				if (route.ID.Equals(ID))
					return route;
			return null;
		}

		public static DateTime DecodeTime(string encodedTime)
		{
			int h, m, s;
			try
			{
				string[] v = encodedTime.Split(':');
				h = Int32.Parse(v[0]);
				m = Int32.Parse(v[1]);
				s = Int32.Parse(v[2]);
			}
			catch (Exception E)
			{ h = 0; m = 0; s = 0; }
			return new DateTime(2000, 1, 1, h, m, s);
		}

		public static string EncodeTime(DateTime time)
		{
			return time.ToString("HH:mm:ss");
		}

		public static TimeSpan DecodeTimeOffset(string encodedTimeSpan)
		{
			DateTime timeRepresentation = DecodeTime(encodedTimeSpan);
			return new TimeSpan(timeRepresentation.Hour, timeRepresentation.Minute, timeRepresentation.Second);
		}

		public static string EncodeTimeOffset(TimeSpan timeOffset)
		{
			return string.Format("{0}:{1}:{2}", timeOffset.Hours.ToString("00"), timeOffset.Minutes.ToString("00"), timeOffset.Seconds.ToString("00"));
		}

		public static string FormatTimeSpan(TimeSpan timeSpan)
		{
			string result = timeSpan.Hours == 0 ? "" : timeSpan.Hours.ToString() + " hrs ";
			result = timeSpan.Minutes == 0 ? result : result + timeSpan.Minutes.ToString() + " min ";
			result = timeSpan.Seconds == 0 ? result : result + timeSpan.Seconds.ToString() + " sec ";
			return result.Equals("") ? "no time" : result.Substring(0, result.Length - 1);
		}
	}

	///
	///
	///

	public class TSettings
	{
		public int TickInterval;
		public TimeSpan TimeOffset;
		public bool DrawStatusLabelRectangles;
	}

	/// 
	/// 
	/// 

	public class TPoint
	{
		public string ID;
		public string Name;
		public double Distance;

		public TPoint(string ID, string Name, double Distance)
		{
			this.ID = ID;
			this.Name = Name;
			this.Distance = Distance;
		}
	}

	public class TRoute
	{
		public string ID;
		public string Name;
		public List<TPoint> Points;

		public TRoute(string ID, string Name)
		{
			this.ID = ID;
			this.Name = Name;
			this.Points = new List<TPoint>();
		}

		public TPoint GetPointByID(string ID)
		{
			foreach (TPoint point in Points)
				if (point.ID.Equals(ID))
					return point;
			return null;
		}
	}

	///
	///
	///

	public class TVehicleLocation
	{
		public TStop PreviousStop = null;
		public TStop NextStop = null;
		public TSegment PreviousSegment = null;
		public TSegment NextSegment = null;
	}

	public class TStop : TVehicleLocation
	{
		public TPoint Point;
		public DateTime Arrival;
		public DateTime Departure;

		public TimeSpan StoppageTime;
		public Rectangle GraphRectangle;

		public TStop(TPoint Point, DateTime Arrival, DateTime Departure)
		{
			this.Point = Point;
			this.Arrival = Arrival;
			this.Departure = Departure;
			this.StoppageTime = TService.TimeSpanSince(Arrival, Departure);
			GraphRectangle = new Rectangle();
		}

		public override string ToString()
		{
			return string.Format("TStop[pointID={0}, arrival={1}, departure={2}, time={3}]", Point.ID, TDatabase.EncodeTime(Arrival), TDatabase.EncodeTime(Departure), TDatabase.FormatTimeSpan(StoppageTime));
		}
	}

	public class TSegment : TVehicleLocation
	{
		public TStop From;
		public TStop To;

		public double Distance;
		public TimeSpan Time;
		public double AvgSpeed;

		public TSegment(TStop From, TStop To)
		{
			this.From = From;
			this.To = To;
			//
			Distance = To.Point.Distance - From.Point.Distance;
			Time = TService.TimeSpanSince(From.Departure, To.Arrival);
			AvgSpeed = (double) Distance / Time.TotalHours;
		}

		public override string ToString()
		{
			return string.Format("TSegment[from={0}, to={1}, distance={2}, time={3}, avgSpeed={4}]",
				From.Point.ID, To.Point.ID, Distance, TDatabase.FormatTimeSpan(Time), AvgSpeed);
		}
	}

	public class TService
	{
		public string ID;
		public string Name;
		public string VehicleName;
		public TRoute Route;
		public List<TStop> Stops;
		public List<TSegment> Segments;
		public TSegment Totals;
		public double MaximumAvgSpeed;

		public TService(string ID, string Name, string VehicleName, TRoute Route)
		{
			this.ID = ID;
			this.Name = Name;
			this.VehicleName = VehicleName;
			this.Route = Route;
			this.Stops = new List<TStop>();
			this.Segments = new List<TSegment>();
			MaximumAvgSpeed = 0;
		}

		public void GenerateSegments()
		{
			for (int i = 0; i < Stops.Count - 1; i++)
				Segments.Add(new TSegment(Stops[i], Stops[i + 1]));

			for (int i = 0; i < Stops.Count ; i++)
			{
				Stops[i].PreviousStop = i == 0 ? null : Stops[i - 1];
				Stops[i].PreviousSegment = i == 0 ? null : Segments[i - 1];
				Stops[i].NextStop = i == Stops.Count - 1 ? null : Stops[i + 1];
				Stops[i].NextSegment = i == Stops.Count - 1 ? null : Segments[i];
			}
			for (int i = 0; i < Segments.Count ; i++)
			{
				Segments[i].PreviousSegment = i == 0 ? null : Segments[i - 1];
				Segments[i].PreviousStop = Stops[i];
				Segments[i].NextSegment = i == Segments.Count - 1 ? null : Segments[i + 1];
				Segments[i].NextStop = Stops[i + 1];
			}

			Totals = new TSegment(Stops[0], Stops[Stops.Count - 1]);

			Totals.Time = Totals.Time.Add(TimeSpanSince(Stops[0].Arrival, Stops[0].Departure));
			Totals.Time = Totals.Time.Add(TimeSpanSince(Stops[Stops.Count - 1].Arrival, Stops[Stops.Count - 1].Departure));

			double totalSpeed = 0;
			foreach (TSegment segment in Segments)
			{
				if (MaximumAvgSpeed < segment.AvgSpeed)
					MaximumAvgSpeed = segment.AvgSpeed;
				totalSpeed += segment.AvgSpeed;
			}
			Totals.AvgSpeed = (double) totalSpeed / Segments.Count;
		}

		public void WriteToFile()
		{
			List<string> lines = new List<string>();
			for (int i = 0; i < Stops.Count - 1; i++)
			{
				lines.Add(Stops[i].ToString());
				lines.Add(Segments[i].ToString());
			}
			lines.Add(Stops[Stops.Count - 1].ToString());
			lines.Add("TOTAL: " + Totals.ToString());
			File.WriteAllLines("temp.txt", lines);
		}

		public static TimeSpan TimeSpanSince(DateTime initialMoment, DateTime targetMoment)
		{
			return targetMoment.Subtract(initialMoment);
		}

		public static double TimePassedPercentage(TimeSpan moment, TimeSpan total)
		{
			int a = moment.Hours * 3600 + moment.Minutes * 60 + moment.Seconds, b = total.Hours * 3600 + total.Minutes * 60 + total.Seconds;
			return (double) (100 * a) / b;
		}
	}
}
